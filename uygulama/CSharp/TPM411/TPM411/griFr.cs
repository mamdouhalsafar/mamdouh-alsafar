﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPM411
{
    public partial class griFr : Form
    {
        Bitmap kaynak, islem;
        public griFr()
        {
            InitializeComponent();
        }

        private void ortalamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y=0;y<yuk;y++)
                {
                    Color renkliPiksel = kaynak.GetPixel(x, y);
                    int griDeger = (renkliPiksel.R + renkliPiksel.G + renkliPiksel.B) / 3;
                    Color griPiksel = Color.FromArgb(griDeger, griDeger, griDeger);
                    islem.SetPixel(x, y, griPiksel);
                }
            }

            islemBox.Image = islem;
        }

        private void bT709ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width; //الحصول على ابعاد المصفوفة
            int yuk = kaynak.Height;//الحصول على ابعاد المصفوفة
            islem = new Bitmap(gen, yuk); // الصورة المعدلة
            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color renk = kaynak.GetPixel(x, y); //الحصول على قيم البيسكل
                    double griDb = renk.R * 0.2125 + renk.G * 0.7154 + renk.B * 0.072;// كود اللوغاريتما
                    int gri = Convert.ToInt32(griDb);
                    Color yeniRenk = Color.FromArgb(gri, gri, gri);
                    islem.SetPixel(x, y, yeniRenk);
                }
            }
            islemBox.Image = islem;
        }

        private void lUMAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width; //الحصول على ابعاد المصفوفة
            int yuk = kaynak.Height;//الحصول على ابعاد المصفوفة
            islem = new Bitmap(gen, yuk); // الصورة المعدلة
            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color renk = kaynak.GetPixel(x, y); //الحصول على قيم البيسكل
                    double griDb = renk.R * 0.3 + renk.G * 0.59 + renk.B * 0.11;// كود اللوغاريتما
                    int gri = Convert.ToInt32(griDb);
                    Color yeniRenk = Color.FromArgb(gri, gri,gri);
                    islem.SetPixel(x, y, yeniRenk);
                }
            }
            islemBox.Image = islem;
        }

        private void acıklıkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);
            Color maxcolor = kaynak.GetPixel(0, 0);
            Color mincolor = kaynak.GetPixel(0, 0);
            int mxr = 0;
            int mnr = 0;
            int mxg = 0;
            int mng = 0;
            int mxb = 0;
            int mnb = 0;
            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color renk = kaynak.GetPixel(x, y);
                    int gri = (renk.R + renk.G + renk.B) / 3;
                    if (renk.R > renk.B)
                    {
                        if (renk.R > renk.G)
                        {
                            mxr = renk.R;
                        }
                    }
                    else if (renk.R > renk.B)
                    {
                        if (renk.R < renk.G)
                        {
                            mxr = renk.G;
                        }
                    }
                    else if (renk.B > renk.R)
                    {
                        if (renk.B > renk.G)
                        {
                            mxr = renk.B;
                        }
                    }
                    else if (renk.B > renk.R)
                    {
                        if (renk.B < renk.G)
                        {
                            mxr = renk.G;
                        }
                    }
                    else if (renk.G > renk.B)
                    {
                        if (renk.G > renk.R)
                        {
                            mxr = renk.G;
                        }
                    }
                    else if (renk.G > renk.R)
                    {
                        if (renk.G < renk.B)
                        {
                            mxr = renk.B;
                        }
                    }

                    //////////////////////////////////////////////////////////////////////////////////////////
                    if (renk.R < renk.B)
                    {
                        if (renk.R < renk.G)
                        {
                            mnr = renk.R;
                        }
                    }
                    else if (renk.R < renk.B)
                    {
                        if (renk.R > renk.G)
                        {
                            mnr = renk.G;
                        }
                    }
                    else if (renk.B < renk.R)
                    {
                        if (renk.B < renk.G)
                        {
                            mnr = renk.B;
                        }
                    }
                    else if (renk.B < renk.R)
                    {
                        if (renk.B > renk.G)
                        {
                            mnr = renk.G;
                        }
                    }
                    else if (renk.G < renk.B)
                    {
                        if (renk.G < renk.R)
                        {
                            mnr = renk.G;
                        }
                    }
                    else if (renk.G < renk.R)
                    {
                        if (renk.G > renk.B)
                        {
                            mnr = renk.B;
                        }
                    }


                    Color mx = Color.FromArgb((mnr + mxr) / 2, (mnr + mxr) / 2, (mnr + mxr) / 2);



                    islem.SetPixel(x, y, mx);
                }
            }
            islemBox.Image = islem;
        }

        private void rennkKanalıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width; //الحصول على ابعاد المصفوفة
            int yuk = kaynak.Height;//الحصول على ابعاد المصفوفة
            islem = new Bitmap(gen, yuk); // الصورة المعدلة
            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color renk = kaynak.GetPixel(x, y); //الحصول على قيم البيسكل
                   //// Color yeniRenk = Color.FromArgb(renk.R, renk.R, renk.R);
                   //// Color yeniRenk = Color.FromArgb(renk.G, renk.G, renk.G);
                    Color yeniRenk = Color.FromArgb(renk.B, renk.B, renk.B);
                    islem.SetPixel(x, y, yeniRenk);
                }
            }
            islemBox.Image = islem;
        }

        private void normalızeEdılmısToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);
            double Ir = 255;
            double Ib = 255;
            double Ig = 255;
            for (int y = 0; y < yuk; y++)
            {
                for (int x = 0; x < gen; x++)
                {
                    Color renk = kaynak.GetPixel(x, y);
                    int T = renk.B + renk.R + renk.G;

                    if (T > 0)
                    {

                        Ir = (255 * (renk.R)) / T;
                        Ib = (255 * (renk.B)) / T;
                        Ig = (255 * (renk.G)) / T;


                    }
                    int Ir1 = Convert.ToInt32(Ir);
                    int Ir2 = Convert.ToInt32(Ib);
                    int Ir3 = Convert.ToInt32(Ig);
                    Color yeniRenk = Color.FromArgb(Ir3, Ir3, Ir3);
                    islem.SetPixel(x, y, yeniRenk);
                }
            }
            islemBox.Image = islem;
        }

        private void açToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                kaynak = new Bitmap(openFileDialog1.FileName);
                kaynakBox.Image = kaynak;
            }
        }
    }
}
